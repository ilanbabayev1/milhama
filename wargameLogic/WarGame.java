/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wargame;

import java.util.ArrayList;
import javax.smartcardio.Card;

/**
 *
 * @author avino
 */
public class WarGame {
      player p1,p2;
    Card p1Card, p2Card;
    Deck theDeck;
    ArrayList <Card> p1Pile, p2Pile; //list to hold cards when war happends
    boolean justWarred; //boolean to know if a war just happened in the game loop
    
    public WarGame()
    {
        justWarred = false;
        p1Pile= new ArrayList<Card>();
        p2Pile= new ArrayList<Card>();
        p1=new player();
        p2=new player();
        String[] suits ={"Hearts","Diamonds","Spades","Clubs"};
        String [] ranks = {"Ace","Two","Three","Four","Five","Six","Seven",
        "Eight","Nine","Ten","Eleven","Twelve","Thirteen"};
        int [] values ={1,2,3,4,5,6,7,8,9,10,12,13};
        theDeck = new Deck (ranks,suits,values);
        theDeck.Shuffle();
        for (int i=0;i<26;i++) //distribute cards to player's hands
        {
            p1.getCard(theDeck.deal());
            p2.getCard(theDeck.deal());
        }
        beginGame();
     
    }
    
    private void beginGame()
    {
        String winner ="";
        while (true)
        {
            pause(100);
            showCard();
            if (!justWarred)//no card on table so each play puts down a card 
                    {
                       if (p1.CardCount()>0)
                           p1Pile.add(p1.playCard());
                       else
                       {
                         winner="p2";//player 1 has no more cards
                         break;
                       } 
                       if (p2.CardCount()>0)
                           p2Pile.add(p2.playCard());
                       else
                       {
                         winner="p1";//player 1 has no more cards
                         break;
                       }
                    }
            justWarred=false;//to make sure player put down cards next round
            p1Card =p1Pile.get(p1Pile.size()-1);
            p2Card =p2Pile.get(p1Pile.size()-1);
                   
            if (p1Card.compareTo(p2Card)>0)
                winner="p1";
            else if(p1Card.compareTo(p2Card)<0)
                winner="p2";
            else 
               winner="none"; 
        }
        
            
        
    }
    
       

 
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
