/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wargame;

import java.util.ArrayList;
import javax.smartcardio.Card;



public class player 
{
    ArrayList<Card> myCards;
    
    public player()
    {
        myCards = new ArrayList<Card>(52); //52 is most they can hold
    }
    
    public void getCard(Card aCard)
    {
        myCards.add(aCard);
    }
    
    public Card playCard()
    {
        return myCards.remove(0);
    } 
    
    public Card seeCard(int position)
    {
        return myCards.get(position);
    }
    
    public int CardCount()
    {
        return myCards.size();
    }
}
