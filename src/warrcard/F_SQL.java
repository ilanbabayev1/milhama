/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warrcard;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *The connection with sql server
 * 
 */

/**
 Accessed sql for trylogin procedure
 */
public class F_SQL {
    
    public int TryLogin(User newLogin){
        
        int result = 0;
        try {
            String userName = "TestingUser"; //*UserName in sql */
            String password = "12345";      //*password in sql*/

            String url = "jdbc:sqlserver://localhost:1433;databaseName=WarrCard";   
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
            conn = DriverManager.getConnection(url,userName,password);
            
            System.out.println("connection created");
            String sql="TryLogin ?,?"; //*Connects to the procedure "Trylogin" */ 
            //* The question marks are to show how many phrases will enter here, in this case it will be  password and Email.*/
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, newLogin.Email); //* Taken from class "User" */
            ps.setString(2, newLogin.Password); //* Taken from class "User" */
            //* What comes first in this case will be Email and after that Password */
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                result = rs.getInt(1);
            }
           
            ps.close();
            conn.close();
        }
        
        /* Catch Sql exception */
        catch(SQLException sqle)
        {
            System.out.println("Sql exception "+sqle);
        }
        
        /* Catch Class NotFound Exception */
        catch (ClassNotFoundException notfe)
        {
             System.out.println("Class Not Found Exception "+notfe);
        }
                
        finally
        {
            
        }             
        return result;
    }
    
    /**
       Accessed sql for InsertUser procedure
      for Register Users
    */
    
    public int Register(Register newRegister){
        
        int result = 0;
        try {
            String userName = "TestingUser"; //*UserName in sql */
            String password = "12345";       //*password in sql*/

            String url = "jdbc:sqlserver://localhost:1433;databaseName=WarrCard";   
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
            conn = DriverManager.getConnection(url,userName,password);
            
            System.out.println("connection created");
            String sql="InsertUser ?,?,?";  //*Connects to the procedure "InsertUser"  */ 
           //* The question marks are to show how many phrases will enter here, in this case it will be users, password and Email.*/
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, newRegister.users); //* Taken from class "Register" */
            ps.setString(2, newRegister.Password); //* Taken from class "Register" */
            ps.setString(3, newRegister.Email);  //* Taken from class "Register" */
            //* What comes first in this case will be Users after that Password and after that Email */
            ps.executeQuery();
            ResultSet rs=ps.getResultSet();
            while(rs.next())
            {
                result = rs.getInt(1);
            }
           
            //ps.close();
            //conn.close();
        }
        
        /* Catch Sql exception */
        catch(SQLException sqle)
        {
            System.out.println("Sql exception "+sqle);
        }
        
        /* Catch Class NotFound Exception */
        catch (ClassNotFoundException notfe)
        {
             System.out.println("Class Not Found Exception "+notfe);
        }
                
        finally
        {
            
        }             
        return result;
    }
}
