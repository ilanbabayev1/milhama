/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warrcard;

/**
 *
 * main class
 */
public class WarrCard {


    public static void main(String[] args) {
        
        F_Client fc = new F_Client(); //*call to the "F_Client" class */
        F_SQL fsql = new F_SQL(); //*call to the "F_SQL" class */
        User UserLogin = new User(); //*call to the "User" class */
        Register REG = new Register(); //*call to the "Register" class */
        int result;
        
        
        switch (fc.HomeScreen())
        {
            case 1: //*When the screen opens if you click "1" to access the login*/
                UserLogin = fc.getLogin();
                result = fsql.TryLogin(UserLogin);
                //*The connection was successful */
                if (result == 1) 
                    System.out.println("login: Successful");
                //*Connection failed*/
                else
                    System.out.println("login: Faild The UserName or Password is incorrect");
                break;
                
            case 2://*When the screen opens if you click "2" to access the Register*/
                REG = fc.register();
                result = fsql.Register(REG);
                //*registration succeeded/*
                if (result == 1)
                    System.out.println("register: Successful");
                //*Registration failed*/
                else
                 System.out.println("register: faild The user name already exists");
        }
    }
    
}
